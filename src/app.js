const express = require('express');
const Promise = require('bluebird');

const app = express();

module.exports = Promise.promisifyAll(app);
