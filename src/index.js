const express = require('express');
const pd = require('paperdrone');

const logger = require('./logger');
const app = require('./app');
const bot = require('./bot');


(async () => {
    if (process.env.PAPERDRONE_WEBHOOK) {
        const prefix = process.env.PAPERDRONE_WEBHOOK;
        bot.configureAndEnable('webhook', {
            'express_router': app,
            'uri_prefix': prefix,
        });

    } else {

        bot.configureAndEnable('polling', {
            'timeout': process.env.PAPERDRONE_POLLING_TIMEOUT || 30,
        });
    }

    await bot.start();

    await app.listenAsync(process.env.PORT);
    logger.debug('listening on: %s', process.env.PORT);
})()
    .catch((err) => console.error(err.stack || err));
