const moment = require('moment-timezone');
const pd = require('paperdrone');

module.exports = pd.Plugin.define('ttp.users', ['commands', 'mongo'], {
    async start (config) {
        const usersColl = this.bot.mongo.collection('users');

        /* Tell owner of new users */
        this.on('command.start', async ($evt, cmd, msg) => {
            const oldUser = await usersColl.findOne({'_id': msg.from.id});

            if (!oldUser) {
                await usersColl.insert({'_id': msg.from.id, 'start_at': moment.utc().toDate()});

                await this.api.sendMessage({
                    'chat_id': process.env.PAPERDRONE_OWNER,
                    'text': `New user: <code>#${msg.from.id}</code> ` +
                        `(@${msg.from.username || '~'}, <a href="tg://user?id=${msg.from.id}">${msg.from.full_name}</a>)`,
                    'parse_mode': 'html',
                    'disable_notification': true,
                });
            }
        });
    },
});
