const emoji = require('node-emoji');
const moment = require('moment-timezone');
const pd = require('paperdrone');

const Charge = require('../utils/charge');
const Title = require('../utils/title');

const {parseDate, formatDateShort, formatDateLong} = require('../utils/utils-dates');

const {FAQ_URL} = require('../const');
const CARD_PHOTO_URL = 'https://www.tarjetatransportepublico.es/CRTM-ABONOS/archivos/img/TTP.jpg';

module.exports = pd.Plugin.define('ttp.cards', ['commands', 'mongo'], {
    async start (config) {
        this.miscColl = this.bot.mongo.collection('misc');
        this.settingsColl = this.bot.mongo.collection('settings');
        this.alertColl = this.bot.mongo.collection('alerts');

        /* Modify your card number */
        this.on('command.setcard', async ($evt, cmd, msg) => {
            await this.bot.prompter.prompt(msg.chat.id, msg.from.id, 'cardNumber');
        });

        /* Check some card status and return full JSON */
        this.on('command._getcard', async ($evt, cmd, msg) => {
            await this.api.sendChatAction({
                'chat_id': msg.chat.id,
                'action': 'typing',
            });

            const cardNumber = cmd.payload || (await this.settingsColl.findOne({'_id': msg.from.id})).card_number;
            const cardResult = await this.bot.ttp.crtm.getCardInfo(cardNumber);

            return new pd.APIRequest('sendMessage', {
                'chat_id': msg.chat.id,
                'text': `\`\`\`\n${JSON.stringify(cardResult, null, 2)}\n\`\`\``,
                'parse_mode': 'Markdown',
            });
        });


        /* Check Current Status */
        this.on('command.info', async ($evt, cmd, msg) => {
            try {
                const when = moment.tz('Europe/Madrid');
                const userSettings = await this.settingsColl.findOne({'_id': msg.from.id});

                const cardNumber = cmd.payload ? cmd.payload.trim() : userSettings.card_number;

                if (!cardNumber) {
                    return new pd.APIRequest('sendMessage', {
                        'chat_id': msg.chat.id,
                        'text': '¡No has configurado tu número de tarjeta!\nUsa /setcard para configurarlo.',
                    });
                }

                await this.api.sendChatAction({
                    'chat_id': msg.chat.id,
                    'action': 'typing',
                });

                this.logger.debug('getting card info (%s)', cardNumber);
                const cardResult = await this.bot.ttp.crtm.getCardInfo(cardNumber);

                /* 0. Errors */
                if (!cardResult.result.ok) {
                    const text = ':warning: Ha ocurrido un error :warning:\n' +
                        `Error #${cardResult.result.code}: ${cardResult.result.message}`;

                    return new pd.APIRequest('sendMessage', {
                        'chat_id': msg.chat.id,
                        'text': emoji.emojify(text),
                    });
                }

                /* 1. Card Info */
                const cardInfo = cardResult.card.info;
                const cardValidStart = parseDate(cardInfo.transport_application_start_date);
                const cardValidEnd = parseDate(cardInfo.transport_application_end_date);
                let messageText = ':credit_card: *Tarjeta*:' +
                    `\n Nº: (${cardInfo.ttp_number_1} ${cardInfo.ttp_number_2} ${cardInfo.ttp_number_3}) ` +
                    `${cardInfo.ttp_number_4} ${cardInfo.ttp_number_5}` +
                    `\n Válida del _${formatDateLong(cardValidStart)}_ al *${formatDateLong(cardValidEnd)}*`;

                /* 2. Profiles */
                messageText += '\n\n :busts_in_silhouette: *Perfiles*:';
                const sortProfileByExpiry = (a, b) => {
                    const dA = parseDate(a.user_profile_expiry_date).valueOf();
                    const dB = parseDate(b.user_profile_expiry_date).valueOf();
                    return dA - dB;
                };
                for (const profile of cardResult.card.profiles.sort(sortProfileByExpiry)) {
                    const profileName = profile.user_profile_type_name;
                    const profileValidStart = parseDate(profile.user_profile_validity_date);
                    const profileValidEnd = parseDate(profile.user_profile_expiry_date);

                    messageText += `\n · *${profileName}* del _${formatDateLong(profileValidStart)}_ ` +
                        `al *${formatDateLong(profileValidEnd)}*`;
                }

                /* 3. Titles */
                for (const titleData of cardResult.titles) {
                    const title = Title.importFromCrtm(titleData);
                    messageText += this.humanizeTitle(title, when);
                }

                messageText += '\n\n`_______________`\n\n:information_source: ' +
                    'Si la información presentada no es correcta o tienes dudas, consulta las ' +
                    `[preguntas frecuentes](${FAQ_URL}) para más información.`;

                return new pd.APIRequest('sendMessage', {
                    'chat_id': msg.chat.id,
                    'text': emoji.emojify(messageText),
                    'parse_mode': 'Markdown',
                    'disable_web_page_preview': true,
                });

            } catch (err) {
                this.logger.error(err);

                await this.bot.feedback.error(err, msg);

                return new pd.APIRequest('sendMessage', {
                    'chat_id': msg.chat.id,
                    'text': 'Ha ocurrido un error al comprobar tu saldo, inténtalo de nuevo más tarde.',
                    'parse_mode': 'Markdown',
                });
            }
        });


        /* Card Number Request */
        this.on('prompt.request.cardNumber', async ($evt, prompt) => {
            const cardPhotoEntry = await this.miscColl.findOne({'_id': 'card_photo_id'});

            const photoMessage = await this.api.sendPhoto({
                'chat_id': prompt.chat,
                'photo': cardPhotoEntry ? cardPhotoEntry.file_id : CARD_PHOTO_URL,
            });

            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'text': 'Introduce el número de tu tarjeta, correspondiente al marcado en la imagen.',
            });

            await this.miscColl.update(
                {'_id': 'card_photo_id'},
                {'file_id': photoMessage.photo[0].file_id},
                {upsert: true},
            );
        });

        this.on('prompt.respond.cardNumber', async ($evt, prompt, response) => {
            await this.api.sendChatAction({
                'chat_id': prompt.chat,
                'action': 'typing',
            });

            const cardNumber = response.text.replace(/\s+/g, '');
            const cardInfo = await this.bot.ttp.crtm.getCardInfo(cardNumber);
            if (!cardInfo) {
                this.api.sendMessage({
                    'chat_id': prompt.chat,
                    'text': 'No parece que ese sea un número de tarjeta válido.\n' +
                        'Vuelve a introducir el número de trajeta. ¡Recuerda introducir solo ' +
                        'los números marcados en la imagen!',
                });

                return pd.plugins.PrompterPlugin.PROMPT_CONTINUE;
            }

            return cardNumber;
        });

        this.on('prompt.complete.cardNumber', async ($evt, prompt, result) => {
            await this.settingsColl.update(
                {'_id': prompt.user},
                {$set: {'card_number': result}},
                {upsert: true},
            );

            await this.alertColl.update(
                {'_id': prompt.user},
                {$set: {'check_at': moment().valueOf()}},
                {upsert: true},
            );

            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'text': `A partir de ahora usaré la tarjeta \`${result}\` para ` +
                    'comprobar tu saldo. Puedes usar /info en cualquier ' +
                    'momento para obtener tu saldo.',
                'parse_mode': 'Markdown',
            });
        });
    },

    humanizeTitle (title, when) {
        const titleEmoji = (title.getType() === Title.TYPE.SUBSCRIPTION)
            ? ':black_small_square:'
            : ':white_small_square:';
        const titleType = title.getType();

        const nameString = `*\n\n${titleEmoji} ${title.name}*`;

        if (titleType === Title.TYPE.SUBSCRIPTION) {
            return `${nameString
            }\n· _Carga_: ${this.humanizeSubscriptionCharge(title.charge, when)}` +
                `\n· _Recarga_: ${this.humanizeSubscriptionCharge(title.recharge, when)}`;
        } else {
            return `${nameString}\n_información no disponible_`;
        }
    },

    humanizeCharge (type, charge, when) {
        switch (type) {
            case Title.TYPE.SUBSCRIPTION:
                return this.humanizeSubscriptionCharge(charge, when);
            case Title.TYPE.CONSUMABLE:
                return this.humanizeConsumableCharge(charge);
            default:
                return '???';
        }
    },

    humanizeSubscriptionCharge (charge, when) {
        const state = charge.getStateAt(when);

        const beginLimit = charge.firstUseLimit ? formatDateShort(charge.firstUseLimit) : null;
        const endLimit = charge.validityEnd ? formatDateShort(charge.validityEnd) : null;
        const firstDay = charge.usageStart ? formatDateShort(charge.usageStart) : null;
        const lastDay = charge.usageEnd ? formatDateShort(charge.usageEnd.add(-1, 'day')) : null;

        switch (state) {
            case Charge.STATE.UNAVAILABLE:
                return 'no disponible';

            case Charge.STATE.ACTIVE:
                return `*activa* desde el _${firstDay}_, último uso el *${lastDay}*`;

            case Charge.STATE.EXPIRED:
                return `*caducada* desde el _${lastDay}_`;

            case Charge.STATE.PENDING:
                return `*pendiente* de activar, fecha límite *${beginLimit}*`;

            case Charge.STATE.OVERDUE:
                return `*límite sobrepasado*, *activa* desde el ${beginLimit}, fecha límite *${endLimit}*`;

            default:
                return ':warning: Ha ocurrido un error :warning:';
        }
    },

    humanizeConsumableCharge (charge) {
        const remaining = charge.remainingUnits;

        if (remaining === null) {
            return 'no disponible';

        } else if (remaining > 0) {
            return 'queda *al menos un uso* disponible';

        } else {
            return 'no queda *ningún uso*';
        }
    },
});
