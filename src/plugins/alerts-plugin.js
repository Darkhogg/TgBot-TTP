const emoji = require('node-emoji');
const moment = require('moment-timezone');
const pd = require('paperdrone');
const Promise = require('bluebird');

const Title = require('../utils/title');
const {formatDateShort} = require('../utils/utils-dates');


module.exports = pd.Plugin.define('ttp.alerts', ['commands', 'mongo', 'prompter'], {
    async start (config) {
        this.usersColl = this.bot.mongo.collection('users');
        this.settingsColl = this.bot.mongo.collection('settings');
        this.alertColl = this.bot.mongo.collection('alerts');

        this.on('command.setalert', async ($evt, cmd, msg) => {
            await this.bot.prompter.prompt(msg.chat.id, msg.from.id, 'expireAlert');
        });

        /* Expiration alert */
        this.on('prompt.request.expireAlert', async ($evt, prompt) => {
            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'text': '¿Con cuánta antelación quieres que te avise de la ' +
                    'caducidad de tu abono?',
                'reply_markup': {
                    'keyboard': [
                        ['5 días', '3 días', '2 días'],
                        ['1 día', 'El mismo día', 'No quiero avisos de caducidad'],
                    ],
                },
            });
        });

        this.on('prompt.respond.expireAlert', async ($evt, prompt, response) => {
            if (response.text.toLowerCase().includes('mismo')) {
                return 0;
            }
            if (response.text.toLowerCase().includes('no')) {
                return null;
            }

            const numeric = parseInt(response.text.replace(/[^0-9]+/g, ' ').trim());
            if (!isNaN(numeric) && numeric >= 0 && numeric <= 5) {
                return numeric;
            }

            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'text': '¡Eso no es una opción válida!',
            });
            return pd.plugins.PrompterPlugin.PROMPT_REPEAT;
        });

        this.on('prompt.complete.expireAlert', async ($evt, prompt, result) => {
            await this.settingsColl.update(
                {'_id': prompt.user},
                {$set: {'alert_expiration': result}},
                {upsert: true},
            );

            if (result === null) {
                await this.alertColl.remove({'_id': prompt.user});

            } else {
                const checkAt = moment.tz('Europe/Madrid').add(20, 'h').set({hours: 4, minutes: 0, seconds: 0});
                await this.alertColl.update(
                    {'_id': prompt.user},
                    {$set: {'check_at': checkAt.valueOf()}},
                    {upsert: true},
                );
            }

            const daysText =
                result === 0 ? '*el mismo día*.'
                    : result === 1 ? 'con *1 día* de antelación.'
                        : `con *${result} días* de antelación.`;

            const messageText = result === null
                ? 'Vale, no te avisaré cuando tus tarjetas vayas a caducar'
                : `Vale, te avisaré de la caducidad ${daysText}`;

            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'text': messageText,
                'parse_mode': 'Markdown',
                'reply_markup': {'remove_keyboard': true},
            });
        });

        this.on('tick', async ($evt, when) => {
            /* we process the expiration always after 6:00 */
            const today = moment(when).set({hours: 6, minutes: 0, seconds: 42});
            const tomorrow = moment(today).add(1, 'day');
            const yesterday = moment(today).subtract(1, 'day');

            this.logger.debug('processing alerts for:', today.toISOString());

            const startTime = moment();

            const alerts = await this.alertColl.find({
                'check_at': {$lt: when.valueOf()},
            }).toArray();

            const stats = {
                'candidates': alerts.length,
                'crtm_calls': 0,
                'alerts_sent': 0,
                'alerts_notified': 0,
                'errors': 0,
                'blocked': [],
            };

            await Promise.map(alerts,
                (alert) => this._processAlert(alert, {today, yesterday, tomorrow}, stats),
                {concurrency: 10});

            const endTime = moment();
            const duration = moment.preciseDiff(startTime, endTime);

            await Promise.map(stats.blocked, async (userId) => {
                const userInfo = await this.api.getChatMember({chat_id: userId, user_id: userId});
                const fullName = [userInfo.user.first_name, userInfo.user.last_name].filter(s => !!s).join(' ');

                this.logger.info('removing user that blocked us: #%s  (%s, @%s)',
                    userId, fullName, userInfo.user.username || '~');
                    
                await this.alertColl.remove({_id: userId});
                await this.settingsColl.remove({_id: userId});
                await this.usersColl.remove({_id: userId});
            });

            this.logger.trace({'alerts_stats': stats}, 'finished alerts processing in:', duration || '0 seconds');
        });
    },

    async _processAlert (alert, days, stats) {
        try {
            const settings = await this.settingsColl.findOne({'_id': alert._id});
            if (!settings) return;

            this.logger.trace({'user_id': alert._id, 'settings': settings, 'alert': alert},
                'checking CTRM info for user %s (card: %s)', alert._id, settings.card_number);

            const cardInfo = await this.bot.ttp.crtm.getCardInfo(settings.card_number);
            stats.crtm_calls += 1;

            const alertMessages = [];
            let alertNotify = false;
            let nextCheckAfterDays = 5;

            let hasActiveTitle = false;
            for (const titleData of (cardInfo.titles || [])) {
                const title = Title.importFromCrtm(titleData);

                const todayState = title.getStateAt(days.today);
                const yesterdayState = title.getStateAt(days.yesterday);


                if (todayState === Title.STATE.ACTIVE_BACKUP || todayState === Title.STATE.ACTIVE_LAST) {
                    hasActiveTitle = true;
                }

                if (todayState === Title.STATE.ACTIVE_LAST) {
                    const lastCharge = title.getLastActiveCharge(days.today);
                    const diffDays = Math.round(lastCharge.usageEnd.diff(days.today, 'days', true));

                    if (diffDays <= settings.alert_expiration) {
                        if (diffDays === 1) {
                            alertNotify = true;
                        }
                        alertMessages.push({
                            'type': 'expires_in',
                            'title': title,
                            'days': diffDays,
                            'expire_date': lastCharge.usageEnd,
                        });
                    }

                    const firstNotifyDays = diffDays - settings.alert_expiration;
                    nextCheckAfterDays = Math.max(firstNotifyDays, 1);
                }

                if (todayState === Title.STATE.ACTIVE_BACKUP) {
                    const lastCharge = title.getLastActiveCharge(days.today);
                    const diffDays = Math.round(lastCharge.usageEnd.diff(days.today, 'days', true));
                    nextCheckAfterDays = Math.max(diffDays, 1);
                }

                if (todayState === Title.STATE.EXPIRED && yesterdayState === Title.STATE.ACTIVE_LAST) {
                    const lastCharge = title.getLastExpiredCharge(days.today);

                    alertNotify = true;
                    alertMessages.push({
                        'type': 'expired_today',
                        'title': title,
                        'expire_date': lastCharge.usageEnd,
                    });
                }

                if (
                    (todayState === Title.STATE.PENDING || todayState === Title.STATE.ACTIVE_LAST) &&
                    yesterdayState === Title.STATE.ACTIVE_BACKUP
                ) {
                    const lastCharge = title.getLastExpiredCharge(days.today);

                    alertMessages.push({
                        'type': 'expired_today_backup',
                        'title': title,
                        'expire_date': lastCharge.usageEnd,
                        'first_use_date': title.recharge.firstUseLimit,
                    });
                }
            }

            if (alertMessages.length) {
                stats.alerts_sent += 1;
                stats.alerts_notified += (alertNotify ? 1 : 0);

                await this.api.sendMessage({
                    'chat_id': alert._id,
                    'text': alertMessages
                        .map((am) => `${this.humanizeTitle(am)}\n${this.humanizeAlertMessage(am)}`)
                        .join('\n\n'),
                    'parse_mode': 'Markdown',
                    'disable_notification': !alertNotify,
                });
            }

            const nextCheckAt = moment.tz(days.today, 'Europe/Madrid')
                .add(Math.max(nextCheckAfterDays, 1), 'd')
                .set({hours: 3})
                .endOf('hour');
            alert.check_at = nextCheckAt.valueOf(); // eslint-disable-line require-atomic-updates
            alert.last_was_active = hasActiveTitle; // eslint-disable-line require-atomic-updates
            await this.alertColl.update(
                {'_id': alert._id},
                {$set: alert},
                {upsert: true},
            );

        } catch (err) {
            if (err.apiResult && err.apiResult.error_code === 403) {
                stats.blocked.push(alert._id);

            } else {
                this.logger.error(err);
                stats.errors += 1; // eslint-disable-line require-atomic-updates
            }
        }
    },

    humanizeTitle (entry) {
        const titleEmoji = (entry.title.getType() === Title.TYPE.SUBSCRIPTION)
            ? ':black_small_square:'
            : ':white_small_square:';
        return emoji.emojify(`*${titleEmoji} ${entry.title.name}*`);
    },

    humanizeAlertMessage (entry) {
        switch (entry.type) {
            case 'expires_in': {
                const lastDay = formatDateShort(entry.expire_date.add(-1, 'day'));
                return (entry.days === 1) ? `¡Tu abono caduca *hoy*!\núltimo día de uso: hoy, *${lastDay}*`
                    : (entry.days === 2) ? `Tu abono caduca *mañana*\núltimo día de uso: mañana, *${lastDay}*`
                        : `Tu abono caduca en *${entry.days - 1} días*\núltimo día de uso: *${lastDay}*`;
            }
            case 'expired_today':
                return '¡Tu abono *ha caducado* y no tienes ninguna recarga!';
            case 'expired_today_backup':
                return 'Tu abono _ha caducado_, pero tienes una recarga.';

            default:
                return '???';
        }
    },
});
