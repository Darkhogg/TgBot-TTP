const pd = require('paperdrone');


module.exports = pd.Plugin.define('ttp.main', ['commands', 'mongo', 'prompter', 'ticking'], {
    async start (config) {
        const info = await this.bot.info();

        this.miscSto = this.bot.mongo.collection('misc');
        this.settingsSto = this.bot.mongo.collection('settings');
        this.alertSto = this.bot.mongo.collection('alerts');

        this.on('command.start', async ($evt, cmd, msg) => {
            await this.api.sendMessage({
                'chat_id': msg.chat.id,
                'text': `Soy @${info.username}, un bot _NO oficial_ que te ayudará a ` +
                    'consultar el saldo de tu Tarjeta Transporte Público (TTP) de Madrid.\n\n' +
                    'Vamos a realizar la configuración inicial...',
                'parse_mode': 'Markdown',
            });

            await this.bot.prompter.prompt(msg.chat.id, msg.from.id, 'cardNumber');
            await this.bot.prompter.prompt(msg.chat.id, msg.from.id, 'expireAlert');
            // await bot.prompter.prompt(msg.chat.id, msg.from.id, 'limitAlert');

            await this.bot.prompter.notify(msg.chat.id, msg.from.id, 'startFinished');
        });

        /* Finished the /start command */
        this.on('prompt.notify.startFinished', async ($evt, prompt) => {
            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'text': '¡Ya hemos acabado! Puedes consultar la ayuda con /help o consultar cómo ' +
                    'modificar las opciones con /settings.\n\n' +
                    'Para informar de un error o enviar consultas, ideas o comentarios utiliza /feedback.\n\n' +
                    'Si quieres estar al tanto de las novedades del bot, únete al canal @TTPMadBotNews.',
                'parse_mode': 'Markdown',
            });
        });

        this.on('command.settings', async ($evt, cmd, msg) => {
            const settings = await this.settingsSto.findOne({'_id': msg.from.id});

            const cardNumber = settings.card_number;
            const cardNumberText = cardNumber
                ? `\`${cardNumber.substring(0, 3)} ${cardNumber.substring(3)}\``
                : 'sin especificar';

            const expireAlert = settings.alert_expiration;
            const expireAlertText =
                (expireAlert === null) ? 'sin alertas'
                    : (expireAlert === 0) ? 'el mismo día'
                        : `${expireAlert} día${expireAlert > 1 ? 's' : ''} antes`;

            const settingsText =
                `*Nº de Tarjeta*: ${cardNumberText}\n/setcard – Cambia tu número de tarjeta\n\n` +
                `*Alertas de Caducidad*: ${expireAlertText}\n/setalert – Cambia los ajustes de alertas\n\n`;

            return new pd.APIRequest('sendMessage', {
                'chat_id': msg.chat.id,
                'text': settingsText,
                'parse_mode': 'Markdown',
            });
        });

        /* leave channels */
        this.on('message', async ($evt, msg) => {
            if (msg.chat.type === 'channel') {
                await this.api.leaveChat({'chat_id': msg.chat.id});
            }
        });
    },
});
