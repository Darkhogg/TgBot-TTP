const emoji = require('node-emoji');
const pd = require('paperdrone');


const FEEDBACK_TYPE = {
    'bug': {emoji: emoji.emojify(':warning:'), name: 'Problema o Fallo'},
    'idea': {emoji: emoji.emojify(':bulb:'), name: 'Idea o Sugerencia'},
    'comment': {emoji: emoji.emojify(':speech_balloon:'), name: 'Consulta o Comentario'},
};


function entities (obj) {
    return String(obj).replace('&', '&amp;').replace('<', '&lt;');
}

module.exports = pd.Plugin.define('ttp.feedback', ['commands', 'callbacks', 'mongo'], {
    async start (config) {
        this.settingsColl = this.bot.mongo.collection('settings');

        this.bot.feedback = {
            error: (...args) => this.reportError(...args),
        };

        this.on('command.feedback', async ($evt, cmd, msg) => this.beginFeedback(msg.chat.id));

        this.on('callback.feedback', async ($evt, cbk) => {
            await this.api.editMessageReplyMarkup({
                'chat_id': cbk.message.chat.id,
                'message_id': cbk.message.message_id,
                'reply_markup': {},
            });

            await this.bot.prompter.prompt(cbk.message.chat.id, cbk.from.id, 'feedbackMessage',
                {'type': cbk.data_payload});
        });

        this.on('prompt.request.feedbackMessage', async ($evt, prompt) => {
            const fbType = FEEDBACK_TYPE[prompt.data.type];
            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'text': `Escribe a continuación tu ${fbType.name} ${fbType.emoji}`,
            });
        });

        this.on('prompt.respond.feedbackMessage', async ($evt, prompt, response) => response);

        this.on('prompt.complete.feedbackMessage', async ($evt, prompt, result) => {
            const user = await this.api.getChat({'chat_id': prompt.user});
            const userFullName = pd.utils.fullName(user.first_name, user.last_name);

            const settings = await this.settingsColl.findOne({'_id': prompt.user});

            await this.api.sendMessage({
                'chat_id': process.env.PAPERDRONE_OWNER,
                'text': '<b>FEEDBACK DE USUARIO</b>' +
                    `\n<b>Tipo</b>: ${FEEDBACK_TYPE[prompt.data.type].emoji} ${FEEDBACK_TYPE[prompt.data.type].name}` +
                    `\n<b>Usuario:</b> <a href="tg://user?id=${user.id}">${entities(userFullName)}</a> (<code>${user.id}</code>)` +
                    `\n<b>Ajustes:</b>\n<pre>${entities(JSON.stringify(settings, null, 2))}</pre>` +
                    `\n\n${entities(result.text)}`,
                'parse_mode': 'html',
                'reply_markup': {
                    'inline_keyboard': [[{
                        text: emoji.emojify(':speech_balloon: Responder'),
                        callback_data: `feedbackreply\0${user.id}\0${result.message_id}`,
                    }]],
                },
            });

            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'text': '¡Gracias! El mensaje se ha enviado al administrador del bot.',
            });
        });

        this.on('callback.feedbackreply', async ($evt, cbk) => {
            if (cbk.from.id != process.env.PAPERDRONE_OWNER) {
                return;
            }


            const [userId, msgId] = cbk.data_payload.split('\0');

            await this.api.editMessageReplyMarkup({
                'chat_id': cbk.message.chat.id,
                'message_id': cbk.message.message_id,
                'reply_markup': {},
            });

            await this.bot.prompter.prompt(cbk.message.chat.id, cbk.from.id, 'feedbackReply', {
                'reply': cbk.message.message_id,
                'orig': msgId,
                'user': userId,
            });
        });

        this.on('prompt.request.feedbackReply', async ($evt, prompt) => {
            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'reply_to_message_id': prompt.data.reply,
                'text': 'Escribe la respuesta a este mensaje',
            });
        });

        this.on('prompt.complete.feedbackReply', async ($evt, prompt, result) => {
            await this.api.sendMessage({
                'chat_id': prompt.data.user,
                'reply_to_message_id': prompt.data.orig,
                'text': entities(result),
                'parse_mode': 'html',
            });

            await this.api.sendMessage({
                'chat_id': prompt.chat,
                'text': 'Respuesta enviada',
            });
        });
    },

    async reportError (error, context) {
        await this.api.sendMessage({
            'chat_id': process.env.PAPERDRONE_OWNER,
            'text': emoji.emojify(':warning: <b>ERROR REPORT</b> :warning:' +
                `\n\n<b>Context</b>\n<pre>${entities(JSON.stringify(context, null, 2))}</pre>` +
                `\n\n<b>Error</b>\n<pre>${entities(error.stack)}</pre>`),
            'parse_mode': 'html',
        });
    },

    async beginFeedback (userId) {
        await this.api.sendMessage({
            'chat_id': userId,
            'text': 'Elige el tipo de mensaje que quieres enviar',
            'reply_markup': {
                'inline_keyboard': Object.entries(FEEDBACK_TYPE).map(([key, {emoji, name}]) => [{
                    callback_data: `feedback\0${key}`,
                    text: `${emoji} ${name}`,
                }]),
            },
        });
    },
});
