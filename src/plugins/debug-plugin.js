const EEE = require('enhanced-event-emitter');
const moment = require('moment-timezone');
const pd = require('paperdrone');
const Promise = require('bluebird');


module.exports = pd.Plugin.define('ttp.debug', ['commands', 'mongo'], {
    async start (config) {
        this.usersColl = this.bot.mongo.collection('users');
        this.miscColl = this.bot.mongo.collection('misc');
        this.settingsColl = this.bot.mongo.collection('settings');
        this.alertColl = this.bot.mongo.collection('alerts');


        this.bot.masters = new Set();
        this.masters = this.bot.masters;

        for (const master of (process.env.PAPERDRONE_MASTERS || '').split(',')) {
            this.masters.add(parseInt(master));
        }

        /* commands starting with _ are admin/debug commands! */
        this.on('command', async ($evt, cmd, msg) => {
            if (cmd.name.indexOf('_') === 0 && !this.masters.has(msg.from.id)) {
                $evt.stop();
            }
        }, EEE.PRIORITY.HIGHER);


        /* Check status for every user and inform them */
        this.on('command._tick', async ($evt, cmd, msg) => {
            const when = moment.tz(cmd.payload || Date.now(), 'Europe/Madrid');
            return this.bot.tick(when);
        });


        /* Broadcast message */
        this.on('command._bcastalert', async ($evt, cmd, msg) => {
            const userIds = await this.bot.mongo.collection('alerts').find().toArray();

            if (!cmd.payload) {
                return this.api.sendMessage({
                    'chat_id': msg.chat.id,
                    'text': 'Cannot broadcast an empty message',
                });
            }

            await this.api.sendMessage({
                'chat_id': msg.chat.id,
                'text': `Broadcasting message to *${userIds.length} users*...`,
                'parse_mode': 'markdown',
            });

            await Promise.map(userIds.map((obj) => obj._id), async (userId) => {
                await this.api.sendMessage({
                    'chat_id': msg.chat.id,
                    'text': cmd.payload,
                    'parse_mode': 'html',
                });
            }, {concurrency: 10});

            return this.api.sendMessage({
                'chat_id': msg.chat.id,
                'text': 'Message broadcast finished',
            });
        });

        /* Clear alerts */
        this.on('command._clearalerts', async ($evt, cmd, msg) => {
            await this.alertColl.update({}, {$set: {'check_at': 0}}, {multi: 1});
        });

        /* Stats on users */
        this.on('command._stats', async ($evt, cmd, msg) => {
            const existingUsers = await this.usersColl.count({});
            const existingCards = await this.settingsColl.count({'card_number': {$exists: true}});
            const activeAlerts = await this.alertColl.count({'check_at': {$gte: moment().valueOf()}});

            await this.api.sendMessage({
                'chat_id': msg.from.id,
                'text': `Existing users: <strong>${existingUsers}</strong>\n` +
                    `Existing cards: <strong>${existingCards}</strong>\n` +
                    `Active alerts: <strong>${activeAlerts}</strong>\n`,
                'parse_mode': 'html',
                'disable_notification': true,
            });
        });
    },
});
