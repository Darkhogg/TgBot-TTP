require('moment-timezone');
require('moment-precise-range-plugin');

const pd = require('paperdrone');

const CRTM = require('./utils/crtm');
const CRTMock = require('./mocks/crtm');

const AlertsPlugin = require('./plugins/alerts-plugin');
const CardsPlugin = require('./plugins/cards-plugin');
const DebugPlugin = require('./plugins/debug-plugin');
const MainPlugin = require('./plugins/main-plugin');
const MongoPlugin = require('./plugins/mongo-plugin');
const UsersPlugin = require('./plugins/users-plugin');
const FeedbackPlugin = require('./plugins/feedback-plugin');

const logger = require('./logger');
const {FAQ_URL} = require('./const');


const bot = pd.createBot(process.env.TELEGRAM_TOKEN, {logger});

bot.ttp = {
    'crtm': process.env.TTPBOT_CRTM_MOCK ? new CRTMock() : new CRTM(),
};

bot.useConfigureAndEnable(MongoPlugin, {
    'uri': process.env.MONGODB_URI || process.env.MONGO_URL || 'mongodb://127.0.0.1/tgbot-ttp',
    'prefix': 'ttp.',
});

bot.configureAndEnable('help', {
    'text': 'Soy @%%USERNAME%%, un bot _NO oficial_ que te ayudará a ' +
        'consultar el saldo de tu Tarjeta Transporte Público (TTP) de Madrid.\n' +
        '\n*Comandos:*\n' +
        ' · /info – Obtiene informacón de tu tarjeta y tu saldo actual.\n' +
        ' · /settings – Consulta o modifica tus ajustes (tarjeta, avisos…).\n' +
        ' · /feedback – Haz llegar al administrador del bot un mensaje informando de un error o ' +
        'con dudas, sugerencias o comentarios.\n' +
        `\nTambién puedes consultar las [preguntas frecuentes](${FAQ_URL}).\n` +
        '\nPara estar al tanto de las novedades, únete a @TTPMadBotNews.',
});

bot.useAndEnable(MainPlugin);
bot.useAndEnable(FeedbackPlugin);

bot.useAndEnable(CardsPlugin);
bot.useAndEnable(AlertsPlugin);

bot.useAndEnable(DebugPlugin);
bot.useAndEnable(UsersPlugin);

module.exports = bot;
