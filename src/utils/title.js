const Charge = require('./charge');

const CHST = Charge.STATE;


class Title {
    constructor (params) {
        this.code = params.code;
        this.name = params.name;
        this.charge = params.charge;
        this.recharge = params.recharge;
    }

    static importFromCrtm (crtmData) {
        return new Title({
            'code': crtmData.info.contract_code,
            'name': crtmData.info.contract_name,
            'charge': Charge.importFromCrtm(crtmData.charge),
            'recharge': Charge.importFromCrtm(crtmData.recharge),
        });
    }

    getType () {
        if (this.code[0] === '1') return Title.TYPE.SUBSCRIPTION;
        if (this.code[0] === '2') return Title.TYPE.CONSUMABLE;
        return Title.TYPE.OTHER;
    }

    getStateAt (when) {
        const chSt = this.charge.getStateAt(when);
        const rechSt = this.recharge.getStateAt(when);

        if (chSt === CHST.UNAVAILABLE && rechSt === CHST.UNAVAILABLE) {
            return Title.STATE.UNAVAILABLE;
        }

        if ((chSt === CHST.ACTIVE && rechSt === CHST.UNAVAILABLE) || rechSt === CHST.ACTIVE) {
            return Title.STATE.ACTIVE_LAST;
        }

        if (chSt === CHST.ACTIVE) {
            return Title.STATE.ACTIVE_BACKUP;
        }

        if (chSt === CHST.PENDING || (chSt === CHST.EXPIRED && rechSt === CHST.PENDING)) {
            return Title.STATE.PENDING;
        }

        if (chSt === CHST.EXPIRED && (rechSt === CHST.UNAVAILABLE || rechSt === CHST.EXPIRED)) {
            return Title.STATE.EXPIRED;
        }

        return null;
    }

    getLastActiveCharge (when) {
        const chSt = this.charge.getStateAt(when);
        const rechSt = this.recharge.getStateAt(when);

        if (rechSt === CHST.ACTIVE) {
            return this.recharge;
        } else if (chSt === CHST.ACTIVE) {
            return this.charge;
        } else {
            return null;
        }
    }

    getLastExpiredCharge (when) {
        const chSt = this.charge.getStateAt(when);
        const rechSt = this.recharge.getStateAt(when);

        if (rechSt === CHST.EXPIRED) {
            return this.recharge;
        } else if (chSt === CHST.EXPIRED) {
            return this.charge;
        } else {
            return null;
        }
    }
}

Title.STATE = Object.freeze({
    UNAVAILABLE: 'unavailable',
    PENDING: 'pending',
    ACTIVE_BACKUP: 'active_backup',
    ACTIVE_LAST: 'active_last',
    EXPIRED: 'expired',
});

Title.TYPE = Object.freeze({
    SUBSCRIPTION: 'subscription',
    CONSUMABLE: 'consumable',
    OTHER: 'other',
});

module.exports = Title;
