const moment = require('moment-timezone');

const {parseDate} = require('./utils-dates');


class Charge {
    constructor (params) {
        this.purchaseDate = params.purchaseDate;
        this.validityStart = params.validityStart;
        this.validityEnd = params.validityEnd;
        this.firstUseLimit = params.firstUseLimit;
        this.usageStart = params.usageStart;
        this.usageEnd = params.usageEnd;
        this.totalUnits = params.totalUnits;
        this.usedUnits = params.usedUnits;
    }

    get remainingUnits () {
        return this.totalUnits === null ? null : this.totalUnits - this.usedUnits;
    }

    static importFromCrtm (crtmData) {
        const params = {
            'purchaseDate': parseDate(crtmData.contract_charge_date || crtmData.contract_recharge_date),
            'validityStart': parseDate(crtmData.contract_charge_start_date || crtmData.contract_recharge_start_date),
            'validityEnd': parseDate(crtmData.contract_charge_end_date || crtmData.contract_recharge_end_date),
            'firstUseLimit': parseDate(crtmData.charge_first_use_date || crtmData.recharge_first_use_date),
            'usageStart': parseDate(crtmData.access_event_in_first_pay_date_ce || crtmData.access_event_in_first_pay_date_rrge),
            'usageEnd': parseDate(crtmData.charge_end_date || crtmData.recharge_end_date),
            'totalUnits': 'charge_remain_units' in crtmData ? parseInt(crtmData.charge_remain_units) : null,
            'usedUnits': 'access_event_units_charge' in crtmData ? Math.abs(parseInt(crtmData.access_event_units_charge)) : null,
        };

        if (params.usageEnd) {
            params.usageEnd.add(30, 'h');
        }

        return new Charge(params);
    }

    getStateAt (when) {
        const whenMoment = moment(when);

        if (!this.validityStart) {
            return Charge.STATE.UNAVAILABLE;
        }

        if (this.usageStart && whenMoment.isBetween(this.usageStart, this.usageEnd)) {
            return Charge.STATE.ACTIVE;
        }

        if (this.usageEnd && whenMoment.isAfter(this.usageEnd)) {
            return Charge.STATE.EXPIRED;
        }

        if (!this.usageStart && whenMoment.isBefore(this.firstUseLimit)) {
            return Charge.STATE.PENDING;
        }

        if (!this.usageStart && !whenMoment.isBefore(this.firstUseLimit)) {
            return Charge.STATE.OVERDUE;
        }

        return null;
    }
}

Charge.STATE = Object.freeze({
    UNAVAILABLE: 'unavailable',
    PENDING: 'pending',
    ACTIVE: 'active',
    OVERDUE: 'overdue',
    EXPIRED: 'expired',
});

module.exports = Charge;
