const moment = require('moment-timezone');


module.exports.parseDate = function parseDate (str) {
    return str ? moment.tz(str, 'Europe/Madrid') : null;
};

module.exports.formatDateShort = function formatDateShort (date) {
    return moment.tz(date, 'Europe/Madrid').locale('es').format('D [de] MMM');
};

module.exports.formatDateLong = function formatDateLong (date) {
    return moment.tz(date, 'Europe/Madrid').locale('es').format('D [de] MMMM [de] YYYY');
};
