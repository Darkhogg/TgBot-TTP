const Promise = require('bluebird');
const request = require('request-promise');
const snakeCase = require('lodash.snakecase');
const xml2js = require('xml2js');

Promise.promisifyAll(xml2js);


const CARD_INFO_URI = 'http://www.citram.es:50081/VENTAPREPAGOTITULO/VentaPrepagoTitulo.svc?wsdl';
const CARD_INFO_SOAPACTION = 'http://tempuri.org/IVentaPrepagoTitulo/ConsultaSaldoTarjeta1';
const CARD_INFO_BODY = `
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
    <soapenv:Header/>
    <soapenv:Body>
        <tem:ConsultaSaldoTarjeta1>
            <tem:sNumeroTP>{{CARD_NUMBER}}</tem:sNumeroTP>
            <tem:sLenguaje>es</tem:sLenguaje>
            <tem:sTipoApp>TELEGRAM_BOT</tem:sTipoApp>
        </tem:ConsultaSaldoTarjeta1>
    </soapenv:Body>
</soapenv:Envelope>`;

const REQUEST_TIMEOUT = 15 * 1000;

class CRTM {
    _attrArrayToObject (arr) {
        const object = {};

        for (const elem of arr) {
            if (elem.name) {
                object[snakeCase(elem.name)] = elem.value;
            }
        }

        return object;
    }

    _requestCardInfo (reqBody) {
        const prom = request({
            'method': 'POST',
            'uri': CARD_INFO_URI,
            'headers': {
                'Content-Type': 'text/xml; charset=utf-8',
                'SOAPAction': CARD_INFO_SOAPACTION,
            },
            'body': reqBody,
            'timeout': REQUEST_TIMEOUT,
        });

        return new Promise((accept, reject) => {
            const tmout = setTimeout(() => reject(new Error('timeout')), REQUEST_TIMEOUT);
            prom.then(accept).catch(reject).finally(() => clearTimeout(tmout));
        });
    }

    async getCardInfo (cardNumber) {
        const xmlReq = CARD_INFO_BODY.replace('{{CARD_NUMBER}}', cardNumber);
        const xmlRes = await this._requestCardInfo(xmlReq);

        const jsonRes = await xml2js.parseStringAsync(xmlRes);

        const resultObject = jsonRes['s:Envelope']['s:Body'][0]['ConsultaSaldoTarjeta1Response'][0]['ConsultaSaldoTarjeta1Result'][0];
        const resultInfo = {
            'ok': resultObject['a:iResultCodeField'][0] === '1',
            'code': resultObject['a:iResultCodeField'][0],
            'message': resultObject['a:iCallLogField'][0],
        };

        if (!resultInfo.ok) {
            return {'result': resultInfo};
        }

        const xmlInfo = resultObject['a:sResulXMLField'][0];

        const jsonInfo = await xml2js.parseStringAsync(xmlInfo);

        const cardInfo = jsonInfo['consulta-saldo']['datos-personalizacion'][0]['data'].map((o) => o.$);
        const cardProfiles = jsonInfo['consulta-saldo']['datos-personalizacion'][0]['perfiles'][0]['perfil']
            .filter((o) => o.data)
            .map((o) => o.data.map((oo) => oo.$));
        const cardDiscounts = jsonInfo['consulta-saldo']['datos-personalizacion'][0]['colectivo'][0]['data']
            .map((o) => o.$);
        const titles = jsonInfo['consulta-saldo']['titulos'][0]['titulo'].filter((o) => o.data).map((o) => ({
            'info': this._attrArrayToObject(o.data.map((oo) => oo.$)),
            'charge': this._attrArrayToObject(o.carga[0].data.map((oo) => oo.$)),
            'recharge': this._attrArrayToObject((o.recarga[0].data || []).map((oo) => oo.$)),
        }));

        return {
            'result': resultInfo,
            'card': {
                'info': this._attrArrayToObject(cardInfo),
                'profiles': cardProfiles.map(this._attrArrayToObject),
                'discounts': this._attrArrayToObject(cardDiscounts),
            },
            'titles': titles,
            'validations': null,
            'inspection': null,
        };
    }
}

module.exports = CRTM;
