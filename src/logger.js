const bunyan = require('bunyan');

module.exports = bunyan.createLogger({
    'name': 'ttpbot',
    'level': 'trace',
});
