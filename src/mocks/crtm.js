const moment = require('moment');

const CHARGE_SPEC = /_C([0-9]{4})-([0-9]{2})-([0-9]{2})-([0-9Xx]+)-([0-9Xx]+)_/;
const RECHARGE_SPEC = /_R([0-9]{4})-([0-9]{2})-([0-9]{2})-([0-9Xx]+)-([0-9Xx]+)_/;

class CRTMock {
    _parseCharge (cardNumber, regex, isRecharge) {
        const matches = regex.exec(cardNumber);
        if (!matches || matches.length < 1) {
            return {};
        }

        const keyName = isRecharge ? 'recharge' : 'charge';
        const accevtName = isRecharge ? 'rrge' : 'ce';

        const purchaseDate = moment({year: parseInt(matches[1]), month: parseInt(matches[2]) - 1, day: parseInt(matches[3])});
        const validityStart = matches[4].toUpperCase() === 'X' ? null : moment(purchaseDate).add(parseInt(matches[4]), 'd');
        const validityEnd = validityStart && moment(validityStart).add(40, 'd');
        const firstUseLimit = validityStart && moment(validityStart).add(9, 'd');
        const usageStart = matches[5].toUpperCase() === 'X' ? null : (validityStart && moment(validityStart).add(parseInt(matches[5]), 'd'));
        const usageEnd = usageStart && moment(usageStart).add(29, 'd');

        return {
            [`contract_${keyName}_date`]: purchaseDate ? purchaseDate.format('YYYY-MM-DD') : '',
            [`contract_${keyName}_start_date`]: validityStart ? validityStart.format('YYYY-MM-DD') : '',
            [`contract_${keyName}_end_date`]: validityEnd ? validityEnd.format('YYYY-MM-DD') : '',
            [`access_event_in_first_pay_date_${accevtName}`]: usageStart ? usageStart.format('YYYY-MM-DD') : '',
            [`${keyName}_first_use_date`]: firstUseLimit ? firstUseLimit.format('YYYY-MM-DD') : '',
            [`${keyName}_end_date`]: usageEnd ? usageEnd.format('YYYY-MM-DD') : '',
            [`${keyName}_remain_units`]: '0',
            [`access_event_units_${keyName}`]: '0',
        };
    }

    async getCardInfo (cardNumber) {
        try {
            const cardInfo = {
                'transport_application_start_date': '1970-01-01',
                'transport_application_end_date': '9999-12-31',
                'ttp_number_1': 'XXX',
                'ttp_number_2': 'YYY',
                'ttp_number_3': 'ZZZ',
                'ttp_number_4': 'WWW',
                'ttp_number_5': 'XMOCKCARDX',
            };
            const cardProfiles = [{
                'user_profile_type_name': 'Mock',
                'user_profile_validity_date': '1970-01-01',
                'user_profile_expiry_date': '9999-12-31',
            }];
            const cardDiscounts = {};

            /* charge / recharge */
            const charge = this._parseCharge(cardNumber, CHARGE_SPEC, false);
            const recharge = this._parseCharge(cardNumber, RECHARGE_SPEC, true);
            if (!charge || !recharge) {
                return null;
            }

            /* create just one title */
            const title = {
                'info': {
                    'contract_code': '1999',
                    'contract_name': 'ABONO MOCK',
                    'contract_user_profile_type': '99',
                    'contract_user_profile_type_name': 'Mock',
                },
                'charge': charge ? charge : (recharge || {}),
                'recharge': recharge || {},
            };

            return {
                'result': {
                    'ok': true,
                },
                'card': {
                    'info': cardInfo,
                    'profiles': cardProfiles,
                    'discounts': cardDiscounts,
                },
                'titles': [title],
                'validations': null,
                'inspection': null,
            };

        } catch (err) {
            return {
                'result': {
                    'ok': false,
                    'code': 'EXC',
                    'message': err.stack,
                },
            };
        }
    }
}

module.exports = CRTMock;
