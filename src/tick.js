const bot = require('./bot');

(async () => {
    try {
        await bot.start();
        await bot.tick();
        await bot.stop();

    } catch (err) {
        console.error(err.stack || err);
    }
})();
