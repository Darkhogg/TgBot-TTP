# @TTPMadBot

[@TTPMadBot][tgbot] es un bot de Telegram *NO oficial* que te ayudará a consultar el saldo de tu
Tarjeta Transporte Público (TTP) de Madrid, así como recibir avisos de caducidad del abono

  [tgbot]: https://telegram.me/TTPMadBot?start=eyJfb3JpZ2luIjoiZ2l0aHViIn0=

Este repositorio contiene todo el código del bot, que se desarrolla como proyecto de código abierto
y acepta contribuciones, pero su principal uso es el gestor de incidencias para fallos.


## Ejecución local

Para probar localmente el proyecto es necesario de disponer de una instalación de Node.js.  También
se necesita el paquete `foreman` instalado globalmente:

    $ npm install -g foreman

Para lanzar el bot es necesario crear un ficher `.env` en la raíz del proyecto, con las siguientes
variables de configuración:

  - `TELEGRAM_TOKEN`: Token del bot a utilizar.  Para poder probar el bot, necesitarás crear un bot
    de prueba.  Habla con [@BotFather](https://telegram.me/BotFather).
  - `PAPERDRONE_OWNER`: ID del usuario propietario del bot.  Este usuario recibirá algunos
    mensajes, como nuevos usuarios o un resumen del proceso de envío de alertas.
  - `PAPERDRONE_MASTERS`: Uno ovarios IDs de usuario, separados por comas, que tendrán permisos
    para lanzar comandos especiales de depuración o administración.

Una vez configuradas estas variables, arranca el bot mediante:

    $ npm -s run babel && nf start --raw | node_modules/.bin/bunyan

De esta manera se lanzará la transpilación del código e inmediatamente después arrancará el bot,
pasando su salida por el comando `bunyan`, que convierte los logs a un formato legible.

Se ha de tener en cuenta que actualmente no existe manera de probar el bot mediante *mocks*, por lo
que será necesario tener disponible un bot de prueba.
