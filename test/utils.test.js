/* global describe, it */
const moment = require('moment-timezone');

require('mocha');
const {expect} = require('chai');

const Charge = require('../src/utils/charge');
const Title = require('../src/utils/title');

/* dates for tests */
const jan01 = moment.tz('2017-01-01', 'Europe/Madrid');
const jan15 = moment.tz('2017-01-15', 'Europe/Madrid');
const feb01 = moment.tz('2017-02-01', 'Europe/Madrid');
const feb15 = moment.tz('2017-02-15', 'Europe/Madrid');
const mar01 = moment.tz('2017-03-01', 'Europe/Madrid');
const mar15 = moment.tz('2017-03-15', 'Europe/Madrid');
const apr01 = moment.tz('2017-04-01', 'Europe/Madrid');


function makeCharge (purchaseDate, validityStart, validityEnd, firstUseLimit, usageStart, usageEnd) {
    return new Charge({purchaseDate, validityStart, validityEnd, firstUseLimit, usageStart, usageEnd});
}

function makeTitle (charge, recharge) {
    return new Title({charge, recharge});
}

const noCharge = makeCharge(null, null, null, null, null, null);
const pendingMarCharge = makeCharge(mar15, mar15, mar15, mar15, null, null);

const activeJanCharge = makeCharge(jan01, jan01, feb01, jan15, jan01, feb01);
const activeJanFebCharge = makeCharge(jan15, jan15, feb15, feb01, jan15, feb15);
const activeFebCharge = makeCharge(feb01, feb01, mar01, feb15, feb01, mar01);
const activeFebMarCharge = makeCharge(feb15, feb15, mar15, mar01, feb15, mar15);
const activeMarCharge = makeCharge(mar01, mar01, apr01, mar15, mar01, apr01);


describe('Charge', () => {
    describe('#getStateAt', () => {
        it('should return UNAVAILABLE for unavailable charges', () => {
            const ch = noCharge;
            expect(ch.getStateAt(jan01)).to.equal(Charge.STATE.UNAVAILABLE);
        });

        it('should return PENDING for inactive charges', () => {
            const ch = pendingMarCharge;
            expect(ch.getStateAt(jan01)).to.equal(Charge.STATE.PENDING);
        });

        it('should return ACTIVE for active charges', () => {
            const ch = activeJanCharge;
            expect(ch.getStateAt(jan15)).to.equal(Charge.STATE.ACTIVE);
        });

        it('should return EXPIRED for expired charges', () => {
            const ch = activeJanCharge;
            expect(ch.getStateAt(feb15)).to.equal(Charge.STATE.EXPIRED);
        });
    });
});

describe('Title', () => {

    describe('#getStateAt', () => {
        it('should return UNAVAILABLE when there are no charges', () => {
            const ttl = makeTitle(noCharge, noCharge);
            expect(ttl.getStateAt(jan01)).to.equal(Title.STATE.UNAVAILABLE);
        });


        it('should return PENDING with one inactive charge', () => {
            const ttl = makeTitle(pendingMarCharge, noCharge);
            expect(ttl.getStateAt(jan01)).to.equal(Title.STATE.PENDING);
        });

        it('should return ACTIVE_LAST with a single active charge', () => {
            const ttl = makeTitle(activeJanCharge, noCharge);
            expect(ttl.getStateAt(jan15)).to.equal(Title.STATE.ACTIVE_LAST);
        });

        it('should return EXPIRED with a single expired charge', () => {
            const ttl = makeTitle(activeJanCharge, noCharge);
            expect(ttl.getStateAt(feb15)).to.equal(Title.STATE.EXPIRED);
        });


        it('should return PENDING with two inactive charges', () => {
            const ttl = makeTitle(pendingMarCharge, pendingMarCharge);
            expect(ttl.getStateAt(jan15)).to.equal(Title.STATE.PENDING);
        });

        it('should return ACTIVE_BACKUP during the first charge', () => {
            const ttl = makeTitle(activeJanCharge, pendingMarCharge);
            expect(ttl.getStateAt(jan15)).to.equal(Title.STATE.ACTIVE_BACKUP);
        });

        it('should return PENDING after an expired charge but before the next', () => {
            const ttl = makeTitle(activeJanCharge, pendingMarCharge);
            expect(ttl.getStateAt(feb15)).to.equal(Title.STATE.PENDING);
        });

        it('should return ACTIVE_LAST during the second charge', () => {
            const ttl = makeTitle(activeJanCharge, activeFebCharge);
            expect(ttl.getStateAt(feb15)).to.equal(Title.STATE.ACTIVE_LAST);
        });

        it('should return EXPIRED with both expired charges', () => {
            const ttl = makeTitle(activeJanCharge, activeFebCharge);
            expect(ttl.getStateAt(mar15)).to.equal(Title.STATE.EXPIRED);
        });
    });
});
